//
//  FirstViewController.swift
//  FisherNet
//
//  Created by Вихляев Сергей on 15.08.2022.
//

import UIKit

final class FirstViewController: UIViewController {
	private let keychainService: KeychainServiceProtocol

	init(keychainService: KeychainServiceProtocol = KeychainService.shared) {
		self.keychainService = keychainService
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		keychainService = KeychainService.shared
		super.init(coder: coder)
	}
}

private extension FirstViewController {

	func firstRoute() {
		routeToAuth(token: keychainService.token)
	}

	private func routeToAuth(token: String?) {
		guard let navigationController = navigationController else { return }
		let authViewController = AuthViewController()
		AuthAssembly(token: token).configure(viewController: authViewController)
		navigationController.present(authViewController, animated: false)
	}
}

