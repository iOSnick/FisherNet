//
//  AuthPresenter.swift
//  FisherNet
//
//  Created by Вихляев Сергей on 15.08.2022.
//

import Foundation

final class AuthPresenter: PresentProtocol {

	// MARK: - PresentProtocol
	weak var view: Viewable?
	var router: Routable?
	var interactor: Interactable?
}

extension AuthPresenter {
	
}
