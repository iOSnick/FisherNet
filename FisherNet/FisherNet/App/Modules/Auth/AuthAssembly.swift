//
//  AuthAssembly.swift
//  FisherNet
//
//  Created by Вихляев Сергей on 15.08.2022.
//

import UIKit

final class AuthAssembly {

	private let token: String?

	init(token: String?) {
		self.token = token
	}
}

// MARK: - AssemblyProtocol
extension AuthAssembly: AssemblyProtocol {
	func configure(viewController: UIViewController) {
		guard var view = viewController as? Viewable else { return }

		var presenter: Presentable = AuthPresenter()
		var interactor: Interactable = AuthInteractor()
		let router: Routable = AuthRouter()

		view.presenter = presenter
		presenter.view = view
		presenter.router = router
		presenter.interactor = interactor
		interactor.presenter = presenter
	}
}
