//
//  AssemblyProtocol.swift
//  FisherNet
//
//  Created by Вихляев Сергей on 15.08.2022.
//

import UIKit

protocol AssemblyProtocol {
	func configure(viewController: UIViewController)
}
