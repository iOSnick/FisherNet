//
//  ViperViewable.swift
//  FisherNet
//
//  Created by Вихляев Сергей on 15.08.2022.
//

typealias Viewable = ViewProtocol & AnyObject
typealias Routable = RouteProtocol & Any
typealias Presentable = PresentProtocol & AnyObject
typealias Interactable = InteractPtocol & Any

protocol ViewProtocol {
	var presenter: Presentable? { get set }
}

protocol PresentProtocol {
	var view: Viewable? { get set }
	var router: Routable? { get set }
	var interactor: Interactable? { get set }
}

protocol InteractPtocol {
	var presenter: Presentable? { get set }
}

protocol RouteProtocol {
	func routeTo(_ target: Any)
}
