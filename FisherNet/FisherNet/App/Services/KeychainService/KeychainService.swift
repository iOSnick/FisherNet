//
//  KeychainService.swift
//  FisherNet
//
//  Created by Вихляев Сергей on 15.08.2022.
//

protocol KeychainServiceProtocol {
	var token: String? { get }
}

final class KeychainService: KeychainServiceProtocol {
	static let shared: KeychainServiceProtocol = KeychainService()

	var token: String?

	private init() {}
}
